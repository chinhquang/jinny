//
//  SignInViewController.swift
//  Jinny
//
//  Created by Chính Trình Quang on 12/18/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import XLPagerTabStrip
class SignInViewController : UIViewController, IndicatorInfoProvider{
    var childNumber: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "\(childNumber)")
    }
    
}
