//
//  TabBarVC.swift
//  Jinny
//
//  Created by Chính Trình Quang on 12/17/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
class TabBarVC : UITabBarController{
    override func viewWillLayoutSubviews() {
        super .viewWillLayoutSubviews()
        var tabFrame:CGRect = self.tabBar.frame
        tabFrame.origin.y = self.view.frame.origin.y
        self.tabBar.frame = tabFrame
        
    }
}
