//
//  SignUpViewController.swift
//  Jinny
//
//  Created by Chính Trình Quang on 12/18/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
class SignUpViewController : UIViewController, IndicatorInfoProvider{
    var childNumber: String = ""
    var isCheck = 0
    let checkedImage = UIImage(named: "ic_checked")! as UIImage
    let uncheckedImage = UIImage(named: "ic_unchecked")! as UIImage
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        UI.addDoneButton(controls: [emailField,passwordField])
    }
    
    @IBAction func checkBoxOnClick(_ sender: Any) {
        if isCheck == 0
        {
            isCheck = 1
            checkBox.setImage(checkedImage, for: .normal)
        }else{
            isCheck = 0
            checkBox.setImage(uncheckedImage, for: .normal)
        }
    }
    @IBOutlet weak var checkBox: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        if isCheck == 0
        {
            let alert  = UIAlertController(title: "You have to agree our Term and Conditions and Privacy Policy to sign up ! ", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
               
            })
            
            present(alert, animated: true, completion: nil)

        }
        else {
            self.requestApi()
        }
    }
    func requestApi (){
        let url :String = "http://jinny.vinova.sg/api/v1/users/sign_up"
        
        let params: [String: Any] = [
            "email": "\(emailField.text ?? "")",
            "password": "\(passwordField.text ?? "")"
            
        ]
        // 3"
        
        Alamofire.request(url, method: .post, parameters: params)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                print("This will print")
                
                let json = response.result.value as! [String: Any]
                
                let status  =  json ["status"] as! Int
                if status == 1 {
                    let result = json["result"] as! [String:Any]
                    _ = Object(json: result)
                    
                }else{
                    print("Sign up unsuccessfully")
                }
        }

    }
    func Notice(value : String, message : String) {
        
        let alert  = UIAlertController(title:  "Status : " + value, message: "Message: " + message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            
        })
        
        present(alert, animated: true, completion: nil)

    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "\(childNumber)")
    }
    
}
